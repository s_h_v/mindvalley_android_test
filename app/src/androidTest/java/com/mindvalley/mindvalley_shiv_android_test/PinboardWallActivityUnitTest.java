package com.mindvalley.mindvalley_shiv_android_test;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.test.ActivityUnitTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import com.mindvalley.mindvalley_shiv_android_test.views.activities.PinboardWallActivity;


/**
 * Created by shiv on 23/1/17.
 */
public class PinboardWallActivityUnitTest extends ActivityUnitTestCase<PinboardWallActivity> {

    private PinboardWallActivity activity;

    public PinboardWallActivityUnitTest() {
        super(PinboardWallActivity.class);
    }


    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Intent intent = new Intent(getInstrumentation().getTargetContext(),
                PinboardWallActivity.class);
        startActivity(intent, null, null);
        activity = getActivity();
    }

    // Sanity check for the layout
    @SmallTest
    public void testLayoutExists() {
        // Verifies the button and text field exist
        assertNotNull(activity.findViewById(R.id.rv_pinboard));
        assertNotNull(activity.findViewById(R.id.fab_dialer));
        assertNotNull(activity.findViewById(R.id.swipelayout_pull_to_refresh));
        assertNotNull(activity.findViewById(R.id.lay_main));
        assertNotNull(activity.findViewById(R.id.pb_loader));
        assertNotNull(activity.findViewById(R.id.toolbar));
    }

    // text field
    @SmallTest
    public void testIntentTriggerViaOnClick() {
        FloatingActionButton mFabDialer = (FloatingActionButton) activity.findViewById(R.id.fab_dialer);
        assertNotNull("Floating action Button should not be null", mFabDialer);
        // Trigger a click on the button
        mFabDialer.performClick();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
