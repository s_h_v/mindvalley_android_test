package com.mindvalley.mindvalley_shiv_android_test.views.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.mindvalley.mindvalley_shiv_android_test.R;
import com.mindvalley.mindvalley_shiv_android_test.model.pojos.UserInformation;
import com.mindvalley.mindvalley_shiv_android_test.utillities.ImageLoader;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by shiv on 22/1/17.
 */
public class PinboardWallAdapter extends RecyclerView.Adapter<PinboardWallAdapter.ViewHolder> {

    Context mContext;
    ArrayList<UserInformation> userInformationArrayList;
    private ImageLoader imageLoader;

    public PinboardWallAdapter(Context mContext, ArrayList<UserInformation> userInformationArrayList) {
        this.userInformationArrayList = userInformationArrayList;
        this.mContext = mContext;
        imageLoader = new ImageLoader(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pinboard_list_item, viewGroup, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.mTvName.setText(userInformationArrayList.get(i).getName());
        viewHolder.mTvId.setText(userInformationArrayList.get(i).getId());
        imageLoader.DisplayImage(userInformationArrayList.get(i).getProfile_pic_url(), viewHolder.cv_user_profile_pic);
    }

    @Override
    public int getItemCount() {
        return userInformationArrayList.size();
    }

    //binding views
    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView cv_user_profile_pic;
        private CardView cardat;
        private TextView mTvName, mTvId;

        public ViewHolder(View view) {
            super(view);
            mTvId = (TextView) view.findViewById(R.id.tv_user_id);
            mTvName = (TextView) view.findViewById(R.id.tv_user_name);
            cv_user_profile_pic = (CircleImageView) view.findViewById(R.id.cv_user_profile_pic);

        }

    }

}
