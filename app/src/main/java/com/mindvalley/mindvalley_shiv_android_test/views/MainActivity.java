package com.mindvalley.mindvalley_shiv_android_test.views;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mindvalley.mindvalley_shiv_android_test.R;
import com.mindvalley.mindvalley_shiv_android_test.model.ModelManager;
import com.mindvalley.mindvalley_shiv_android_test.model.PinboardApiManager;
import com.mindvalley.mindvalley_shiv_android_test.model.pojos.ObservableBean;
import com.mindvalley.mindvalley_shiv_android_test.utillities.Utils;
import com.mindvalley.mindvalley_shiv_android_test.views.adapter.PinboardWallAdapter;

import java.util.Observable;
import java.util.Observer;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by shiv on 22/1/17.
 */
public class MainActivity extends AppCompatActivity implements Observer, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    @Bind(R.id.rv_pinboard)
    RecyclerView mPinboardListRecycler;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.pb_loader)
    ProgressBar mProgressLoader;

    @Bind(R.id.lay_main)
    CoordinatorLayout mCordinatorParent;

    @Bind(R.id.swipelayout_pull_to_refresh)
    SwipeRefreshLayout mPullToRefreshLayout;

    @Bind(R.id.fab_dialer)
    FloatingActionButton mFabDialer;

    private Context mContext;

    private PinboardApiManager pinboardApiManager;
    private boolean isRefreshing = false;

    private static final int PERMISSION_REQUEST_CODE = 1;

    /**
     * Permissions required to read and write contacts..
     */
    private static String[] PERMISSIONS_STORAGE = {android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    //this function  bind the views and set some data like title of toolbar and color schemes of pull to refresh layout
    private void initialize() {
        mContext = this;
        ButterKnife.bind(this);
        pinboardApiManager = ModelManager.getInstance().getPinboardApiManager();
        pinboardApiManager.addObserver(this);
        mPullToRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimaryDark);
        mPullToRefreshLayout.setOnRefreshListener(this);
        mFabDialer.setOnClickListener(this);
        setToolbarData();
        triggerDataAPiForData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermission();
        if (pinboardApiManager != null)
            pinboardApiManager.addObserver(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (pinboardApiManager != null)
            pinboardApiManager.deleteObserver(this);
    }

    /*
     *set title for toolbar
     */
    private void setToolbarData() {
        mToolbar.setTitle(getString(R.string.toolbar_title));
        setSupportActionBar(mToolbar);
    }

    /*
     *call pinboardapi manager and calls the api if internet is available else show error message
     */
    private void triggerDataAPiForData() {
        if (Utils.isNetworkAvailable(this)) {
            if (!isRefreshing) {
                mProgressLoader.setVisibility(View.VISIBLE);
            }
            pinboardApiManager.callApiAndGetData();
        } else {
            mProgressLoader.setVisibility(View.GONE);
            Utils.showSnackBar(mCordinatorParent, getString(R.string.no_internet));
        }
    }


    @Override
    public void update(Observable observable, final Object objectReceived) {
        final ObservableBean observableBean = (ObservableBean) objectReceived;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isRefreshing) {
                    mPullToRefreshLayout.setRefreshing(false);
                    isRefreshing = false;
                }
                mProgressLoader.setVisibility(View.GONE);
                //casting object type
                if (observableBean != null) {
                    if (observableBean.isDataAvailable()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setDataInPiboardList();
                            }
                        });
                    } else {
                        Utils.showSnackBar(mCordinatorParent, getString(R.string.no_data_available));
                    }
                } else {
                    Utils.showSnackBar(mCordinatorParent, getString(R.string.no_data_available));
                }
            }
        });

    }

    /*
    *This function set the adapter to recycler view
     */
    private void setDataInPiboardList() {
        if (ModelManager.getInstance().getCacheManager().getUserInformationArrayList().size() > 0) {
            Log.e("size", ModelManager.getInstance().getCacheManager().getUserInformationArrayList().size() + "");
            LinearLayoutManager layoutManager1 = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            mPinboardListRecycler.setLayoutManager(layoutManager1);
            PinboardWallAdapter pinboardWallAdapter = new PinboardWallAdapter(mContext, ModelManager.getInstance().getCacheManager().getUserInformationArrayList());
            mPinboardListRecycler.setAdapter(pinboardWallAdapter);
        } else {
            Utils.showSnackBar(mCordinatorParent, getString(R.string.no_data_available));
        }
    }

    @Override
    public void onRefresh() {
        isRefreshing = true;
        triggerDataAPiForData();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fab_dialer:
                Utils.showSnackBar(mCordinatorParent, getString(R.string.snackmessage));
                break;

        }

    }

    /**
     * Requests the storage permission.
     */
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            requestPermission();
            return false;
        }
    }

    /**
     * If storage is not enabled, it requests for
     * the storage permission to be enabled
     */
    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Utils.showSnackBar(mCordinatorParent, getString(R.string.permission_granted_to_access_app));
                    setDataInPiboardList();
                } else {
                    Utils.showSnackBar(mCordinatorParent, getString(R.string.permission_denied_to_access_app));
                    checkPermission();
                }
                break;
        }
    }

}
