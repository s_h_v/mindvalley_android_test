package com.mindvalley.mindvalley_shiv_android_test.controller;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by shiv on 22/1/17.
 */
public class ApplicationGlobalController extends Application {


    private static ApplicationGlobalController singleton;


    public static ApplicationGlobalController getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        init();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    private void init() {
        MyVolley.init(this);
    }
}
