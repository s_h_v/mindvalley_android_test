package com.mindvalley.mindvalley_shiv_android_test.model.pojos;

/**
 * Created by shiv on 22/1/17.
 */
public class UserInformation {


    String id, profile_pic_url,name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfile_pic_url() {
        return profile_pic_url;
    }

    public void setProfile_pic_url(String profile_pic_url) {
        this.profile_pic_url = profile_pic_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
