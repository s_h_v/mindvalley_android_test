package com.mindvalley.mindvalley_shiv_android_test.model;

/**
 * Created by shiv on 22/1/17.
 */
public class ModelManager {

    private static ModelManager ourInstance = new ModelManager();

    public static ModelManager getInstance() {
        return ourInstance;
    }
    private CacheManager cacheManager;
    private PinboardApiManager pinboardApiManager;
    private ModelManager() {
        cacheManager = new CacheManager();
        pinboardApiManager= new PinboardApiManager();

    }

    /**
     * @return returns Cache manager
     */
    public CacheManager getCacheManager() {
        if (cacheManager == null)
            cacheManager = new CacheManager();
        return cacheManager;
    }
    /**
     * @return returns Pinboard manager
     */
    public PinboardApiManager getPinboardApiManager() {
        if (pinboardApiManager == null)
            pinboardApiManager = new PinboardApiManager();
        return pinboardApiManager;
    }




}
