package com.mindvalley.mindvalley_shiv_android_test.model;

import com.mindvalley.mindvalley_shiv_android_test.model.pojos.UserInformation;

import java.util.ArrayList;

/**
 * Created by shiv on 22/1/17.
 */
public class CacheManager {

    private ArrayList<UserInformation> userInformationArrayList;

    /*
    * this function returns the arraylist of user information type data for caching
     */
    public ArrayList<UserInformation> getUserInformationArrayList() {
        return userInformationArrayList;
    }

    /*
    * this function used to set the userinformation after iteration and parsing the data
     */
    public void setUserInformationArrayList(ArrayList<UserInformation> userInformationArrayList) {
        this.userInformationArrayList = userInformationArrayList;
    }
}
