package com.mindvalley.mindvalley_shiv_android_test.model.pojos;

public class ObservableBean {
    private boolean isDataAvailable;


    public boolean isDataAvailable() {
        return isDataAvailable;
    }

    public void setDataAvailable(boolean dataAvailable) {
        isDataAvailable = dataAvailable;
    }
}
