package com.mindvalley.mindvalley_shiv_android_test.model;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mindvalley.mindvalley_shiv_android_test.controller.MyVolley;
import com.mindvalley.mindvalley_shiv_android_test.model.pojos.ObservableBean;
import com.mindvalley.mindvalley_shiv_android_test.model.pojos.UserInformation;
import com.mindvalley.mindvalley_shiv_android_test.utillities.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Observable;


/**
 * Created by shiv on 22/1/17.
 */
public class PinboardApiManager extends Observable {

    private ObservableBean observableBean = new ObservableBean();
    private ArrayList<UserInformation> userInformationArrayList;

    /*
    *This function call api through volley ( a gogle's libraray for rest interaction) and get response in json/xml format
     */
    public void callApiAndGetData() {
        RequestQueue queue = MyVolley.getRequestQueue();
        StringRequest myReq = new StringRequest(Request.Method.GET,
                Constants.BASE_URL+"raw/wgkJgazE",
                createMyReqSuccessListener(),
                createMyReqErrorListener());
        queue.add(myReq);

    }


    /*
    *This function triggers on request sucess
     */

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", response);
                parseResponseAndSetToArrayList(response);
            }
        };
    }

    /*
       *This function triggers on request failure or error
        */
    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                triggerOnFalse();
            }
        };
    }

    /*
    * this function trigger and notify the UI element in case there is a failure and no data available
     */
    private void triggerOnFalse() {
        observableBean.setDataAvailable(false);
        triggerObservers(observableBean);
    }

    /*
    *This function  checks the validity if input is json type and parse the response and fill array list which is responsible for update the pinboard wall list
    * @param response string type argument passed which  consists response
     */
    private void parseResponseAndSetToArrayList(String response) {
        if (!response.isEmpty()) {
            //checks if input is a valid json or not
            if (isJSONValid(response)) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    if (mainArray.length() > 0) {
                        observableBean.setDataAvailable(true);
                        userInformationArrayList = new ArrayList<>();
                        for (int i = 0; i < mainArray.length(); i++) {
                            UserInformation userInformation = new UserInformation();
                            JSONObject mainDataObject = mainArray.getJSONObject(i);
                            if (mainDataObject.has("user")) {
                                JSONObject userObject = mainDataObject.getJSONObject("user");
                                userInformation.setId(userObject.has("id") ? userObject.getString("id") : "");
                                userInformation.setName(userObject.has("name") ? userObject.getString("name") : "");
                                if (userObject.has("profile_image")) {
                                    JSONObject profileImageObject = userObject.getJSONObject("profile_image");
                                    userInformation.setProfile_pic_url(profileImageObject.has("medium") ? profileImageObject.getString("medium") : "");
                                }
                            }
                            userInformationArrayList.add(userInformation);
                        }
                        //set users data after parsing
                        ModelManager.getInstance().getCacheManager().setUserInformationArrayList(userInformationArrayList);
                        triggerObservers(observableBean);
                    } else {
                        triggerOnFalse();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    triggerOnFalse();
                }
            } else {

                //parse for another type of data like XML

                //Mxl parsing -pull, dom , sax


            }
        } else {
            triggerOnFalse();
        }
    }


    /*
    * This function returns true if data is  of type json or not
     */

    public boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    /**
     * It triggers observers to notify the success and failure.
     *
     * @param success an object that is obtained as a response of
     *                hitting api. It notifies the corresponding
     *                observer.
     */

    private void triggerObservers(final Object success) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                setChanged();
                notifyObservers(success);
            }
        }).start();


    }

}
