package com.mindvalley.mindvalley_shiv_android_test.utillities;

import android.content.Context;

import java.io.File;

/**
 * Created by shiv on 21/1/17.
 */
public class FileCache {
    
    private File cacheDir;
    
    public FileCache(Context context){
        //Find the directory for saving cached images
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"LazyList");
        else
            cacheDir=context.getCacheDir();
        if(!cacheDir.exists())
            cacheDir.mkdirs();
    }
    
    public File getFile(String url){
        // identify images by hashcode for duplicacy issue
        String filename= String.valueOf(url.hashCode());
        File f = new File(cacheDir, filename);
        return f;
        
    }
    
    public void clear(){
        File[] files=cacheDir.listFiles();
        if(files==null)
            return;
        for(File f:files)
            f.delete();
    }

}